package cryptchat.toolkit;

/**
 *
 * @author Chris
 */
import cryptchat.server.CryptChatServer;
import java.io.*;
import java.net.*;
public class ServerThread extends Thread
{
    // The Server that spawned us
    private CryptChatServer server;
    // The Socket connected to our client
    private Socket socket;
    // Constructor.
    public ServerThread( CryptChatServer server, Socket socket ) {
        // Save the parameters
        this.server = server;
        this.socket = socket;
        // Start up the thread
        start();
    }
    // This runs in a separate thread when start() is called in the
    // constructor.
    public void run() {
        try {
            // Create a DataInputStream for communication; the client
            // is using a DataOutputStream to write to us
            BufferedReader input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            
            
            // Over and over, forever ...
            while (true) {
                // ... read the next message ...
                String message = input.readLine();
                // ... tell the world ...
                if(message == null)
                    throw new EOFException();
                
                // ... and have the server send it to all clients
                server.sendToAll( message );
            }
        } catch( EOFException ie ) {
            // This doesn't need an error message
            server.removeConnection( socket );
        } catch( IOException ie ) {
            // This does; tell the world!
            ie.printStackTrace();
        }
    }
}