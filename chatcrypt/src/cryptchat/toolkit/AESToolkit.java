/*
 * Copyright (C) 2013 Chris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cryptchat.toolkit;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Chris
 */
public class AESToolkit{

  public static void main(String[] args) throws UnsupportedEncodingException {
    try {

      String key = "ThisIsASecretKey";
      byte[] ciphertext = encrypt(key, "Hello MAN, ante kai kalh tuxh");
      System.out.println("decrypted value:" + (decrypt(key, ciphertext)));

    } catch (GeneralSecurityException e) {
      e.printStackTrace();
    }
  }

  public static byte[] encrypt(String key, String value)
      throws GeneralSecurityException {

    byte[] raw = null;
      try {
          raw = key.getBytes("UTF-8");
      } catch (UnsupportedEncodingException ex) {
          Logger.getLogger(AESToolkit.class.getName()).log(Level.SEVERE, null, ex);
      }
    if (raw.length != 16) {
      throw new IllegalArgumentException("Invalid key size.");
    }

    SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec,
        new IvParameterSpec(new byte[16]));
      try {
          return cipher.doFinal(value.getBytes("UTF-8"));
      } catch (UnsupportedEncodingException ex) {
          Logger.getLogger(AESToolkit.class.getName()).log(Level.SEVERE, null, ex);
      }
      return new byte[16];
  }

  public static String decrypt(String key, byte[] encrypted)
      throws GeneralSecurityException, UnsupportedEncodingException {

    byte[] raw = null;
      try {
          raw = key.getBytes("UTF-8");
      } catch (UnsupportedEncodingException ex) {
          Logger.getLogger(AESToolkit.class.getName()).log(Level.SEVERE, null, ex);
      }
    if (raw.length != 16) {
      throw new IllegalArgumentException("Invalid key size.");
    }
    SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");

    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    cipher.init(Cipher.DECRYPT_MODE, skeySpec,
        new IvParameterSpec(new byte[16]));
    byte[] original = cipher.doFinal(encrypted);

    return new String(original, "UTF-8");
  }
}