/*
 * Copyright (C) 2013 Chris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cryptchat.toolkit;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chris
 */
public class RSAToolkit {
    
    public static void main(String args[]){
        int something;
        something = generatePair();
    }
    
    public static int generatePair(){
        int status = 0;
        
        KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048);
            KeyPair kp = kpg.genKeyPair();
            
            KeyFactory fact = KeyFactory.getInstance("RSA");
            RSAPublicKeySpec pub = fact.getKeySpec(kp.getPublic(),
              RSAPublicKeySpec.class);
            RSAPrivateKeySpec priv = fact.getKeySpec(kp.getPrivate(),
              RSAPrivateKeySpec.class);

            saveToFile("public.key", pub.getModulus(), pub.getPublicExponent());
            saveToFile("private.key", priv.getModulus(), priv.getPrivateExponent());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            status = 1;
            Logger.getLogger(RSAToolkit.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return status;
    }
    
    public static void saveToFile(String fileName, BigInteger mod, BigInteger exp) {
        try{
            ObjectOutputStream oout = new ObjectOutputStream( new BufferedOutputStream( new FileOutputStream(fileName)));
            try {
              oout.writeObject(mod);
              oout.writeObject(exp);
            } catch (IOException e) {
              throw new IOException("Unexpected error", e);
            } finally {
              oout.close();
            }
        }
        catch(IOException ex){
            Logger.getLogger(RSAToolkit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
